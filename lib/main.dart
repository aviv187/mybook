import 'package:flutter/material.dart';

// import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;

import './pages/loginPage.dart';

// Future main() async {
//   await DotEnv.load(fileName: ".env");
// }
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'IronClic Test',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LoginPage(),
    );
  }
}
