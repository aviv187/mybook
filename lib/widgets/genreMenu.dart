import 'package:flutter/material.dart';

class GenreMenu extends StatefulWidget {
  final TextEditingController genreController;

  GenreMenu(this.genreController);

  @override
  _GenreMenuState createState() => _GenreMenuState();
}

class _GenreMenuState extends State<GenreMenu> {
  List<String> genreList = [
    'Fiction',
    'Action & Adventure',
    'Crime & Mystery',
    'Fantasy',
    'Horror',
    'Romance',
    'Sci-Fi',
    'Historical Novel',
    'Other',
  ];

  String selection = 'pick a genre';

  void selectGenre(genre) {
    setState(() {
      selection = genre;
    });
    widget.genreController.text = genre;
  }

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      onSelected: (genre) {
        selectGenre(genre);
      },
      child: Text(
        selection,
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.bold,
          color: Theme.of(context).primaryColor,
        ),
      ),
      itemBuilder: (context) => genreList.map((genre) {
        return PopupMenuItem(
          value: genre,
          child: Text(genre),
        );
      }).toList(),
    );
  }
}
