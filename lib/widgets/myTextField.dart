import 'package:flutter/material.dart';

// my costum textField
class MyTextField extends StatelessWidget {
  final TextEditingController controller;
  final String title;
  final bool isPassword;
  final int maxLine;

  MyTextField({
    @required this.controller,
    @required this.title,
    this.isPassword = false,
    this.maxLine = 1,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // close the keyboard
      onDoubleTap: () => FocusScope.of(context).unfocus(),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: TextField(
          maxLines: maxLine,
          obscureText: isPassword,
          controller: controller,
          decoration: InputDecoration(
            floatingLabelBehavior: FloatingLabelBehavior.always,
            border: OutlineInputBorder(),
            labelText: title,
          ),
        ),
      ),
    );
  }
}
