import 'package:flutter/material.dart';

class StarRatingBar extends StatelessWidget {
  final double rating;
  final Function onRatingChanged;
  final double size;

  StarRatingBar({
    @required this.rating,
    this.onRatingChanged,
    this.size = 35,
  });

  final int starCount = 5;

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: List.generate(
          starCount,
          (index) => BuildStar(
            index: index,
            rating: rating,
            onRatingChanged: onRatingChanged,
            size: size,
          ),
        ));
  }
}

// build evey star by how full it shpuld be
class BuildStar extends StatelessWidget {
  final int index;
  final double rating;
  final Function onRatingChanged;
  final double size;

  BuildStar(
      {@required this.index,
      @required this.rating,
      @required this.onRatingChanged,
      @required this.size});

  @override
  Widget build(BuildContext context) {
    final color = Theme.of(context).primaryColor;
    Icon icon;

    // check how much of the star to fill
    if (index >= rating) {
      icon = Icon(
        Icons.star_border,
        color: Theme.of(context).buttonColor,
        size: size,
      );
    } else if (index > rating - 1 && index < rating) {
      icon = Icon(
        Icons.star_half,
        color: color,
        size: size,
      );
    } else {
      icon = Icon(
        Icons.star,
        color: color,
        size: size,
      );
    }

    return Tooltip(
      message: 'double tap for half a star',
      child: InkResponse(
        // make half a star
        onDoubleTap: () => onRatingChanged(index + 0.5),
        // make full star
        onTap: () => onRatingChanged(index + 1.0),
        child: icon,
      ),
    );
  }
}
