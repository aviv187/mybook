import 'package:flutter/material.dart';

import './bookCard.dart';
import '../models/bookModel.dart';

class BooksList extends StatelessWidget {
  const BooksList(this.bookList);

  final List<Book> bookList;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      // list of all the books
      child: ListView(
        children: bookList.map((book) {
          return BookCard(book);
        }).toList(),
      ),
    );
  }
}
