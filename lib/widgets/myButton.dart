import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final String title;
  final Function function;
  final double size;
  final Color color;

  MyButton({
    @required this.title,
    @required this.function,
    this.size = 14,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      child: OutlinedButton(
        onPressed: function,
        child: Container(
          padding: EdgeInsets.all(10),
          child: Text(
            title,
            style: TextStyle(
              color: (color == null) ? Theme.of(context).primaryColor : color,
              fontSize: size,
            ),
          ),
        ),
      ),
    );
  }
}
