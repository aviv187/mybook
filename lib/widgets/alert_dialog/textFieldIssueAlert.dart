import 'package:flutter/material.dart';

class TextFieldIssueAlert extends StatelessWidget {
  final String errorText;

  TextFieldIssueAlert(this.errorText);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Oops!'),
      content: Text(errorText),
      actions: [
        TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Ok'),
        ),
      ],
    );
  }
}
