import 'package:flutter/material.dart';

import '../../pages/booksPage.dart';

class ContinAsGuestDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Are you sure you want to continue as guest?'),
      content:
          Text('If you\'d continue as guest you\'d be unable to add books'),
      actions: [
        TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('no'),
        ),
        TextButton(
          onPressed: () {
            Navigator.pop(context);
            // navigate to books page unauthorized
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => BooksPage(false)),
            );
          },
          child: Text('yes'),
        ),
      ],
    );
  }
}
