import 'package:flutter/material.dart';

import '../models/bookModel.dart';
import './starRatingBar.dart';

class BookCard extends StatefulWidget {
  final Book book;

  BookCard(this.book);

  @override
  _BookCardState createState() => _BookCardState();
}

class _BookCardState extends State<BookCard> {
  int maxLine = 2;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // open/close the card func by increasing the max line for the summery
      onTap: () {
        setState(() {
          (maxLine == 2) ? maxLine = 9999 : maxLine = 2;
        });
      },
      // the card itself
      child: Card(
        color: Colors.grey[200],
        margin: EdgeInsets.all(10),
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Row(
                children: [
                  // title
                  Text(
                    widget.book.title,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    ),
                  ),
                  // author
                  Text(
                    ' by ' + widget.book.author,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 14,
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                ],
              ),
              // summery
              Container(
                alignment: Alignment.topLeft,
                padding: EdgeInsets.all(5),
                child: Text(
                  widget.book.summary,
                  overflow: TextOverflow.ellipsis,
                  maxLines: maxLine,
                  style: TextStyle(fontSize: 14),
                  textAlign: TextAlign.start,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //rating
                  StarRatingBar(
                    rating: widget.book.rating,
                    size: 16,
                  ),
                  // genre
                  Text(
                    widget.book.genre,
                    style: TextStyle(fontSize: 12),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
