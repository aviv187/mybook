import 'package:flutter/material.dart';

import '../widgets/myTextField.dart';
import '../widgets/starRatingBar.dart';
import '../widgets/genreMenu.dart';
import '../widgets/myButton.dart';
import '../widgets/alert_dialog/textFieldIssueAlert.dart';
import '../help_func/dataApiHelper.dart';
import '../pages/booksPage.dart';

class AddBookPage extends StatefulWidget {
  @override
  _AddBookPageState createState() => _AddBookPageState();
}

class _AddBookPageState extends State<AddBookPage> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _autherController = TextEditingController();
  final TextEditingController _summeryController = TextEditingController();
  final TextEditingController _genreController = TextEditingController();

  double rating = 3.5;
  // function fot the star sating (change the number of stars)
  void onRatingChanged(newRating) {
    setState(() => rating = newRating);
    rating = newRating;
  }

  // check that all the fields have content
  bool checkFieldsAreFull() {
    if (fixString(_titleController.text, '') == '' ||
        fixString(_autherController.text, '') == '' ||
        fixString(_summeryController.text, '') == '' ||
        fixString(_genreController.text, '') == '') {
      return false;
    }

    return true;
  }

  // send the book to the database
  void addBook() {
    if (checkFieldsAreFull()) {
      DataApiHelper.instance.sendBook(
        title: fixString(_titleController.text, '+'),
        author: fixString(_autherController.text, '+'),
        summary: fixString(_summeryController.text, '+'),
        genre: fixString(_genreController.text, '+'),
        rating: rating,
      );

      Navigator.pop(context);
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => BooksPage(true)),
      );
    } else {
      // ask the user to fill all field
      showDialog(
        context: context,
        builder: (_) => TextFieldIssueAlert('not all fields are filled'),
      );
    }
  }

  // make all the spaces in the string to + for the api
  // or erase all the space from a string (to check the string is not only spaces)
  // depend on the secont parameter
  String fixString(String text, String joinBy) {
    final List<String> arrHolder = text.split(' ');
    final String fixedText = arrHolder.join(joinBy);
    return fixedText;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // for the keyboard popup
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Add a Book'),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            MyTextField(title: 'Title', controller: _titleController),
            MyTextField(title: 'Author', controller: _autherController),
            MyTextField(
              title: 'Summary',
              controller: _summeryController,
              maxLine: 10,
            ),
            GenreMenu(_genreController),
            StarRatingBar(rating: rating, onRatingChanged: onRatingChanged),
            MyButton(title: 'Save Book', function: addBook)
          ],
        ),
      ),
    );
  }
}
