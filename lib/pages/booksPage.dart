import 'dart:async';

import 'package:flutter/material.dart';

import './addBookPage.dart';
import './loginPage.dart';
import '../models/bookModel.dart';
import '../widgets/booksList.dart';
import '../widgets/alert_dialog/textFieldIssueAlert.dart';
import '../help_func/dataApiHelper.dart';

class BooksPage extends StatefulWidget {
  final bool isAuth;

  BooksPage(this.isAuth);

  @override
  _BooksPageState createState() => _BooksPageState();
}

class _BooksPageState extends State<BooksPage> {
  List<Book> bookList = [];

  Timer getBooksTimer;

  // set timer for the app to get the books from the database
  void _startTimer() {
    getBooksTimer = Timer(Duration(seconds: 10), () {
      showDialog(
        context: context,
        builder: (_) => TextFieldIssueAlert(
            'we could not get the books\nyou might not have an internet connection'),
      );
    });
  }

  // fench the books from the api
  void _getAllBooks() async {
    final List<Book> futueBookList = await DataApiHelper.instance.fetchBooks();

    if (futueBookList.isNotEmpty) {
      getBooksTimer.cancel();
    }

    setState(() {
      bookList = futueBookList;
    });
  }

  void logout() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  @override
  void initState() {
    super.initState();

    _getAllBooks();
    _startTimer();
  }

  @override
  void dispose() {
    getBooksTimer?.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Books'),
        // menu buttom for logout and more in the future
        // it will show only if you are auth
        actions: widget.isAuth
            ? [
                PopupMenuButton(
                  onSelected: (func) {
                    func();
                  },
                  child: Icon(Icons.menu),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      value: logout,
                      child: Text("Logout"),
                    ),
                  ],
                ),
                // make the button farther from the edge
                SizedBox(width: 10)
              ]
            : [],
      ),
      // the body of the books page will be a Circular Progress Indicator until we fetch the books from the database
      body: bookList.isEmpty
          ? Center(child: CircularProgressIndicator())
          : BooksList(bookList),
      // navigate to add book page button
      // it will show only if you are auth or if could not connect to the api
      floatingActionButton: (widget.isAuth && bookList.isNotEmpty)
          ? FloatingActionButton(
              child: Icon(Icons.edit),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddBookPage()),
                );
              },
            )
          : null,
    );
  }
}
