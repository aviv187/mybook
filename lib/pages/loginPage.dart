import 'package:flutter/material.dart';

import 'signupPage.dart';
import './booksPage.dart';
import '../widgets/myTextField.dart';
import '../widgets/alert_dialog/continueAsGuestDialog.dart';
import '../widgets/myButton.dart';

class LoginPage extends StatelessWidget {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // for the keyboard popup
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Login Page'),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // title of the app
            Container(
              margin: EdgeInsets.all(30),
              child: Center(
                child: FittedBox(
                  fit: BoxFit.fitWidth,
                  child: Text(
                    'Book\nRecommendations',
                    style: TextStyle(
                      fontSize: 45,
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
            // email textfield
            MyTextField(title: 'Username/Email', controller: _emailController),
            // password textfiels
            MyTextField(
              title: 'Password',
              controller: _passwordController,
              isPassword: true,
            ),
            // login buttom
            MyButton(
              title: 'Login',
              function: () {
                // TODO: handle login
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => BooksPage(true)),
                );
              },
              size: 40,
            ),
            // a box to put distance between the login and the other buttons
            SizedBox(height: 20),
            // signup buttom
            MyButton(
              color: Colors.black,
              title: 'Signup',
              function: () {
                // switch to the singin page
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SignupPage()),
                );
              },
            ),
            // continue as guest
            MyButton(
              color: Colors.black,
              title: 'Continue as guest',
              function: () {
                // check with the user that he is sure he want to continue as guest before moving to the books page
                showDialog(
                  context: context,
                  builder: (_) => ContinAsGuestDialog(),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
