import 'package:flutter/material.dart';

import 'booksPage.dart';
import '../widgets/myTextField.dart';
import '../widgets/myButton.dart';
import '../widgets/alert_dialog/textFieldIssueAlert.dart';

class SignupPage extends StatelessWidget {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _vPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    bool checkEmail() {
      // check if the email is valid by cherecters only (without email confirmation)
      bool isEmailValid = RegExp(
              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
          .hasMatch(_emailController.text);

      if (!isEmailValid) {
        showDialog(
          context: context,
          builder: (_) => TextFieldIssueAlert('This is not a valid email'),
        );
      }

      return isEmailValid;
    }

    bool checkUserName() {
      bool isUsernameInvalid =
          RegExp('[^A-Za-z0-9]').hasMatch(_usernameController.text);

      // check that username is not too short/long
      if (4 > _usernameController.text.length ||
          _usernameController.text.length > 16) {
        isUsernameInvalid = true;
      }

      if (isUsernameInvalid) {
        showDialog(
          context: context,
          builder: (_) => TextFieldIssueAlert(
              'This is not a valid username \na username should be composed from 4-16 letters and numbers'),
        );
      }

      return !isUsernameInvalid;
    }

    bool checkPassword() {
      // check that password is not too short/long
      bool isPasswordValid = (6 > _passwordController.text.length ||
              _passwordController.text.length > 20)
          ? false
          : true;

      // check that the password match in both fields
      bool isPasswordMacth =
          _passwordController.text == _vPasswordController.text;

      if (!isPasswordValid) {
        showDialog(
          context: context,
          builder: (_) => TextFieldIssueAlert(
              'This is not a valid password \na password should be composed from 4-16 characters'),
        );
      } else if (!isPasswordMacth) {
        showDialog(
          context: context,
          builder: (_) => TextFieldIssueAlert('The passwords does not match'),
        );
      }

      isPasswordMacth
          ? isPasswordValid = isPasswordValid
          : isPasswordValid = false;
      return isPasswordValid;
    }

    void checkUser() {
      // if all is valid we can create new user and continue to book page
      if (checkEmail() && checkUserName() && checkPassword()) {
        // TODO: handle auth
        Navigator.pop(context);
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => BooksPage(true)),
        );
      }
    }

    return Scaffold(
      // for the keyboard popup
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Sign up'),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Center(
              child: Container(
                margin: EdgeInsets.all(20),
                child: Text(
                  'Create a new user',
                  style: TextStyle(fontSize: 25),
                ),
              ),
            ),
            // a column of the signin textfields
            Column(
              children: [
                MyTextField(title: 'Email', controller: _emailController),
                MyTextField(title: 'Username', controller: _usernameController),
                // password textfield, just with a little joke
                Tooltip(
                  message:
                      'do not reuse your bank password\nthis app is not that secure',
                  child: MyTextField(
                    title: 'Password',
                    controller: _passwordController,
                    isPassword: true,
                  ),
                ),
                MyTextField(
                  title: 'Verify Password',
                  controller: _vPasswordController,
                  isPassword: true,
                ),
              ],
            ),
            // create a new user buttom
            MyButton(
              title: 'Create New User',
              function: checkUser,
            ),
          ],
        ),
      ),
    );
  }
}
