import 'package:flutter/material.dart';

class Book {
  final String title;
  final String author;
  final String summary;
  final String genre;
  final double rating;

  Book({
    @required this.title,
    @required this.author,
    @required this.rating,
    @required this.genre,
    @required this.summary,
  });
}
