import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../models/bookModel.dart';

const String endpoint = String.fromEnvironment('ENDPOINT');

class DataApiHelper {
  final String url = endpoint != '' ? endpoint : 'http://localhost:3000/';

  DataApiHelper._privateConstractur();
  static final DataApiHelper instance = DataApiHelper._privateConstractur();

  // get the books from the api
  Future<List<Book>> fetchBooks() async {
    final Uri fetchUrl = Uri.parse('${url}get');

    final response = await http.get(fetchUrl);

    if (response.statusCode == 200) {
      // convert json to object
      final List<dynamic> rawBooks = jsonDecode(response.body);
      List<Book> booksList = [];

      // convert object to book class
      rawBooks.forEach((bookData) {
        try {
          final Book book = Book(
            title: bookData['title'],
            author: bookData['author'],
            // dividing rating by 1 to convert it to double
            rating: double.parse(bookData['rating']),
            genre: bookData['genre'],
            summary: bookData['summary'],
          );

          booksList.add(book);
        } catch (e) {
          throw e;
        }
      });

      return booksList;
    } else {
      throw Exception('Failed to load books');
    }
  }

  // sending book to the api
  Future sendBook({
    @required String title,
    @required String author,
    @required String summary,
    @required String genre,
    @required double rating,
  }) async {
    // setting the url with the book parameters
    final Uri fetchUrl = Uri.parse(
      url +
          'add?title=$title&author=$author&summary=$summary&genre=$genre&rating=$rating',
    );

    final response = await http.get(fetchUrl);

    if (response.statusCode != 200) {
      throw Exception('Failed to send book');
    }
  }
}
