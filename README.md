# book_app

An app for people who likes to read.

In this app people can share books that they read with summery and rating.

To add books to the list you need to create a user, if you want to continue as a guest you will only be able to see the books, but not to add a book yourself.

## How to build?

For development purposes use the following build command:

  flutter build web

You can specify an endpoint during build, for example, building for staging:

  flutter build web --dart-define=ENDPOINT=https://heroku-gitlab-aviv-staging.herokuapp.com/

For docker version use / as endpoint:

  flutter build web --dart-define=ENDPOINT=/

If you do not specify ENDPOINT, the build will use localhost:3000